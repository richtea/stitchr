import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import React, { Component, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  Button,
} from "react-native";
import {
  ScrollView,
  PanGestureHandler,
  State,
} from "react-native-gesture-handler";
import { PinchGestureHandler } from "react-native-gesture-handler";
import ReactNativeZoomableView from "@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView";
import { Canvas } from "./components/Canvas";
import { ColourBar } from "./components/ColourBar";

export const App = () => {
  const [selectedColour, setSelectedColour] = useState<any>({ hex: "blue" });
  const [userColours] = useState([{ hex: "blue" }]);

  return (
    <View style={{ flex: 1, height: "100%" }}>
      <View style={{ flex: 1, width: "100%" }}>
        <ReactNativeZoomableView
          maxZoom={5}
          minZoom={0.5}
          zoomStep={0.75}
          initialZoom={1}
          bindToBorders={true}
        >
          <Canvas selectedColour={selectedColour.hex} />
        </ReactNativeZoomableView>
      </View>
      <ColourBar
        onSelectedColourChange={(colour) => {
          setSelectedColour(colour);
        }}
        selectedColour={selectedColour}
        userColours={userColours}
      />
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
