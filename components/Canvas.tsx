import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import { Pixel } from "./Pixel";

interface IProps {
  selectedColour: string;
}
interface Cell {
  colour: string;
}

export const Canvas = (props: IProps) => {
  const [data, setData] = useState<Array<Array<Cell>>>();
  useEffect(() => {
    var x = new Array(100);

    for (var i = 0; i < x.length; i++) {
      x[i] = new Array(100);
    }
    setData(x);
  }, []);

  if (data === undefined) {
    return null;
  }

  return (
    <View>
      {[...Array(100)].map((x, row) => (
        <View style={styles.row} key={`row-${row}`}>
          {[...Array(100)].map((x, col) => (
            <Pixel
              selectedColour={props.selectedColour}
              key={`${row}-${col}`}
              colour={
                data[row][col] != undefined ? data[row][col].colour : "white"
              }
              onPress={() => {
                console.log("boop");
                // const newData = [...data];
                // newData[row] = [...data[row]];
                // newData[row][col] = {
                //   colour: "orange"
                // };
                // setData(newData);
                console.log("bop");
              }}
            />
          ))}
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    flexWrap: "nowrap",
    padding: 0,
  },
});
