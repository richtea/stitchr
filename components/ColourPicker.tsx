import React from "react";
import { ListItem } from "react-native-elements";
import { colours } from "../assets/floss";
import { View, Button, FlatList } from "react-native";

interface IProps {
  onClose: (colour?: any) => void;
}

export const ColourPicker = (props: IProps) => {
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={colours}
        keyExtractor={(item) => item.floss}
        renderItem={({ item, index }) => (
          <ListItem
            key={item.floss || index}
            leftIcon={
              <View
                style={{
                  height: 20,
                  width: 50,
                  marginRight: 10,
                  backgroundColor: item.hex,
                }}
              />
            }
            title={item.name}
            subtitle={item.floss}
            bottomDivider
            onPress={() => props.onClose(item)}
          />
        )}
      />
      <Button title="Close" onPress={() => props.onClose()} />
    </View>
  );
};
