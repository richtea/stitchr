import React, { useState, useEffect } from "react";
import { StyleSheet, View, TouchableHighlight } from "react-native";

interface IProps {
  selectedColour: string;
}
export const Pixel = (props) => {
  const [colour, setColour] = useState<string>("white");
  useEffect(() => {
    if (props.colour) {
      setColour(props.colour);
    }
  }, [props.colour]);
  return (
    <View
      style={[styles.square, { backgroundColor: colour }]}
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => {
        setColour(props.selectedColour);
        props.onPress();
      }}
    />
  );
};

const styles = StyleSheet.create({
  square: {
    width: 10,
    height: 10,
    backgroundColor: "#eee",
    borderColor: "#ccc",
    borderWidth: 1,
  },
});
