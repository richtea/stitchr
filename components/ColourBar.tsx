import React, { useEffect, useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import { View, FlatList } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Overlay } from "react-native-elements";
import { ColourPicker } from "./ColourPicker";

interface IProps {
  onSelectedColourChange: (colour?: any) => void;
  userColours: Array<any>;
  selectedColour: any;
}
export const ColourBar = (props: IProps) => {
  const [pickerOpen, setPickerOpen] = useState<boolean>(false);
  const [colours, setColours] = useState<Array<any>>([]);
  useEffect(() => setColours(props.userColours), [props.userColours]);

  return (
    <View
      style={{
        flexDirection: "row",
        width: "100%",
        height: 60,
        backgroundColor: "rgba(255,255,255,0.8)",
      }}
    >
      <FlatList
        data={colours}
        horizontal
        contentContainerStyle={{
          alignItems: "center",
          marginLeft: 10,
          marginRight: 20,
        }}
        keyExtractor={(item) => item.hex}
        ListFooterComponent={() => (
          <TouchableOpacity onPress={() => setPickerOpen(true)}>
            <Ionicons
              name="md-add"
              size={40}
              color="grey"
              style={{ marginLeft: 20 }}
            />
          </TouchableOpacity>
        )}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={() => props.onSelectedColourChange(item)}>
            <View
              style={{
                height: item.hex === props.selectedColour.hex ? 30 : 15,
                width: item.hex === props.selectedColour.hex ? 30 : 15,
                marginLeft: 10,
                marginRight: 10,
                backgroundColor: item.hex,
                borderRadius: 20,
              }}
            />
          </TouchableOpacity>
        )}
      />
      {pickerOpen && (
        <Overlay isVisible={true}>
          <ColourPicker
            onClose={(colour) => {
              setColours([...colours, colour]);
              props.onSelectedColourChange(colour || props.selectedColour);
              setPickerOpen(false);
            }}
          />
        </Overlay>
      )}
    </View>
  );
};
